package com.example.coordinatorsample.floatingactionbutton

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.coordinatorsample.R


/**
 * Created by sergi on 26/04/2022.
 * Copyright (c) 2022 Qastusoft. All rights reserved.
 */

class FabEnCabecera : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fab_cabecera)
    }
}