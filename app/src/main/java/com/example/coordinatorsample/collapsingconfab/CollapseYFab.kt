package com.example.coordinatorsample.collapsingconfab

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.coordinatorsample.Cheeses
import com.example.coordinatorsample.adapter.CheeseAdapter
import com.example.coordinatorsample.databinding.CollapsingFabBinding


/**
 * Created by sergi on 26/04/2022.
 * Copyright (c) 2022 Qastusoft. All rights reserved.
 */

class
CollapseYFab : AppCompatActivity() {

    private lateinit var binding: CollapsingFabBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = CollapsingFabBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupRecyclerView()

        setSupportActionBar(binding.toolbar)
        title = "Floating Action Button over Collapsing Toolbar"
    }

    private fun setupRecyclerView() {
        val recyclerView = binding.recyclerview
        val listaQuesos = Cheeses.getRandomSublist(30)

        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
        recyclerView.adapter = CheeseAdapter(listaQuesos)
    }
}