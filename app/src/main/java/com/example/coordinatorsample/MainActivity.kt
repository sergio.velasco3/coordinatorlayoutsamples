package com.example.coordinatorsample

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import com.example.coordinatorsample.bottomappbar.BottomAppBarActivity
import com.example.coordinatorsample.collapsingconfab.CollapseYFab
import com.example.coordinatorsample.collapsingtoolbar.CollapseCase1
import com.example.coordinatorsample.collapsingtoolbar.CollapseCase2
import com.example.coordinatorsample.collapsingtoolbar.CollapseCase3
import com.example.coordinatorsample.collapsingtoolbar.CollapseCase4
import com.example.coordinatorsample.databinding.ActivityMainBinding
import com.example.coordinatorsample.floatingactionbutton.FabConSnackYScroll
import com.example.coordinatorsample.floatingactionbutton.FabEnCabecera
import com.example.coordinatorsample.floatingactionbutton.FabExpandido
import com.example.coordinatorsample.parallax.ParallaxEfect
import com.example.coordinatorsample.tablayout.ScrollTabLayout
import com.example.coordinatorsample.toolbarsola.ToolbarEnterAlways
import com.example.coordinatorsample.toolbarsola.ToolbarScroll
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        WindowCompat.setDecorFitsSystemWindows(window, true)
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        binding.fab.setOnClickListener {
            Snackbar.make(it, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAnchorView(R.id.fab)
                .setAction("Action", null).show()
        }

        val content = binding.content

        content.button1.setOnClickListener {
            fabAlertDialog()
        }

        content.button3.setOnClickListener {
            openActivity(BottomAppBarActivity::class.java)
        }

        content.button4.setOnClickListener {
            toolbarAlertDialog()
        }

        content.button6.setOnClickListener {
            colllapsingAlertDialog()
        }

        content.button10.setOnClickListener {
            openActivity(ScrollTabLayout::class.java)
        }

        content.button11.setOnClickListener {
            openActivity(CollapseYFab::class.java)
        }

        content.button12.setOnClickListener {
            openActivity(ParallaxEfect::class.java)
        }
    }


    fun fabAlertDialog() {
        val lista = arrayOf("FAB hiding/showing con SnackBar", "Fab extendido", "FAB anclado entre vistas")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("FAB Samples")
        builder.setItems(lista) { dialog, which ->
            if (which == 0) {
                openActivity(FabConSnackYScroll::class.java)
            }else if (which == 1) {
                openActivity(FabExpandido::class.java)
            } else {
                openActivity(FabEnCabecera::class.java)
            }
        }

        builder.create().show()
    }

    fun toolbarAlertDialog() {
        val lista = arrayOf("Toolbar son scroll", "Toolbar con snterAlways")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("FAB Samples")
        builder.setItems(lista) { dialog, which ->
            if (which == 0) {
                openActivity(ToolbarScroll::class.java)
            } else {
                openActivity(ToolbarEnterAlways::class.java)
            }
        }

        builder.create().show()
    }

    fun colllapsingAlertDialog() {
        val lista = arrayOf(
            "Collapsing scroll|enterAlways",
            "Collapsing scroll|enterAlwaysCollapsed",
            "Collapsing scroll|enterAlways|enterAlwaysCollapsed",
            "Collapsing scroll|exitUntilCollapsed"
        )

        val builder = AlertDialog.Builder(this)
        builder.setTitle("FAB Samples")
        builder.setItems(lista) { dialog, which ->
            when (which) {
                0 -> openActivity(CollapseCase1::class.java)
                1 -> openActivity(CollapseCase2::class.java)
                2 -> openActivity(CollapseCase3::class.java)
                3 -> openActivity(CollapseCase4::class.java)
            }
        }

        builder.create().show()
    }

    fun openActivity(clase: Class<*>) {
        startActivity(Intent(this, clase))
    }


}